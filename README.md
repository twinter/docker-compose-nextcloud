# Nextcloud server with collabora office/document server

## Download and Setup

1. clone the repo somewhere (we will assume it is in /home/user/docker/nextcloud and the active account is user from now on) with `git clone https://gitlab.com/twinter/docker-compose-nextcloud.git ~/docker/nextcloud`

2. create .env file to match your system based on the provided template file



## Startup

### With the [docker-compose systemd service](https://gitlab.com/twinter/docker-compose-systemd-service) (recommended as a permanent setup)

1. set up the docker-compose systemd service as described here: https://gitlab.com/twinter/docker-compose-systemd-service

2. create a symlink from the projects folder to /etc/docker/compose with `sudo ln -s /home/user/docker/nextcloud /etc/docker/compose/nextcloud`

3. start and enable the service with `sudo systemctl enable --now docker-compose@nextcloud`

### With only docker-compose

Run `sudo docker-compose up` in the projects folder on your system.



## Configuration

1. set up nextcloud with the built-in assistant (just visit the website it should be reachable on)

2. install the following apps (click on the letter in the top right corner):
	- Collabora Online

3. set the following options in the nextcloud settings (letter in the top right corner):
	- enable cron (basic settings -> background jobs)
	- set up you mailserver (basic settings -> email server)
	- set the URL and port of your office server (Collabora online -> top of the page), "https://" at the beginning and ":443" at the end

4. if you get content security policy (CSP) errors (these mostly look like random stuff couldn't be loaded): you may need to add the following settings manually to your config.php (see blow), see https://docs.nextcloud.com/server/18/admin_manual/configuration_server/config_sample_php_parameters.html#proxy-configurations for details:
    - 'overwritehost'
    - 'overwriteprotocol'
    - 'overwritewebroot'

You may skip these the following steps as none of these are be required. They are mostly minor tuning.

### proper configuration of Redis

Nextcloud should already be configured to use redis.

Feel free to have a look at https://docs.nextcloud.com/server/17/admin_manual/configuration_server/caching_configuration.html for the options for your config.php (see below on where to find it).
It is recommended to keep on using APCu for local caching if you have enough memory. Use Redis for distributed caching and file locking.

### install some apps

I recommend to have following additional apps installed but all of these are optional:
- OCC Web
- Auditing / Logging
- Calendar
- Checksum
- Contacts
- Mail
- Metadata
- Quota warning
- Tasks



## Maintenance

### Nextcloud container

#### Configuration

You can either set internal variables with the occ (see below) or access the configuration files in /srv/nextcloud/app/config.

#### Access to the occ (nextcloud control terminal)

There are (at least) two options:

*Easy option*:

Install the "OCC Web" app and use it.

*Harder option*:

Run `sudo docker-compose exec -u www-data app php occ <occ command>` in the folder with the docker-compose.yml file in it

*Hardest Option*:

Connect to the container with `sudo docker-compose exec -u www-data app bash` from the folder with the docker-compose.yml file in it.

You may need to enable logging in as www-data if it doesn't work. To do this log in normally `sudo docker-compose exec app bash` and change the terminal for www-user in /etc/passwd to /bin/bash. Do this with `cat` and `sudo echo […] > /etc/passwd`.

Run `php occ [command]`.

### mysql/mariadb

You may find some usefull information in the nextcloud docs.

#### Backup

Run the following command and the backup will be on /srv/nextcloud/db on the host.
```
mysqldump --single-transaction -h localhost -u nextcloud --password=supersecretpassword nextcloud > /var/lib/mysql/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
```

#### Restore

```
mysql -h localhost -u nextcloud -psupersecretpassword -e "DROP DATABASE nextcloud"
mysql -h localhost -u nextcloud -psupersecretpassword -e "CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci"
mysql -h localhost -u nextcloud -psupersecretpassword nextcloud < [backup file]
```

#### Recover from database failure

- put nextcloud in maintenance mode (`occ maintenance:mode --on`)
- add `--innodb-force-recovery=[level]` to the starting command in the docker-compose file, see https://mariadb.com/kb/en/library/innodb-recovery-modes/ for details on the levels. Remember to back up your db folder!
- increment the levels and try to get the database up
- pull the backup as soon as the database is running again

Load the backup into a fresh instance of the database container if you encountered the "log sequence number in the future" error.
